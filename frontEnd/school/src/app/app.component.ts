import {Component, OnDestroy, OnInit} from '@angular/core';
import {ChildrenService} from "./services/children.service";
import {AllRoute} from "./models/allRoute";
import {takeUntil, tap} from "rxjs/operators";
import {Subject, Subscription} from "rxjs";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit,OnDestroy{
route:AllRoute[] = this.childrenService.allRoute;
  children:string;
  message:string;
  destroy$ :Subject<boolean> = new Subject<boolean>();
  constructor(private childrenService :ChildrenService) {
  }
  ngOnInit() {
    this.childrenService.handleChildren$
      .pipe(
        takeUntil(this.destroy$),
        tap((message:string)=>this.message = message)
      )
      .subscribe();
  }
  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  manageChildren(children){
    this.children = children;
  }

}
