import { Injectable } from '@angular/core';
import {AllRoute} from "../models/allRoute";
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ChildrenService {
message: string;
  allRoute: AllRoute[] = [{url:'children',name:'children'}, {url:'teacher',name:'teacher'}];
  handleChildren$ :BehaviorSubject<string> = new BehaviorSubject<string>(null);

  constructor() {}
  setHandleChildren$(message: string){
    this.handleChildren$.next(message);
  }
}


