export interface ProfessorModel {
    nom:string;
    prenom:string;
    matière:string;
    age:number;
    actif:boolean;
}
