import {Component, Input, OnInit, Output} from '@angular/core';
import {ChildrenService} from "../../services/children.service";

@Component({
  selector: 'app-menu1',
  templateUrl: './menu1.component.html',
  styleUrls: ['./menu1.component.scss']
})
export class Menu1Component implements OnInit {
  @Input() route;

  constructor() {}

  ngOnInit(): void {
  }
}
