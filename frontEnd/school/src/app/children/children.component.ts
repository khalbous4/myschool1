import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {BehaviorSubject} from "rxjs";
import {ChildrenService} from "../services/children.service";

@Component({
  selector: 'app-children',
  templateUrl: './children.component.html',
  styleUrls: ['./children.component.scss']
})
export class ChildrenComponent implements OnInit {

  constructor(private childrenService : ChildrenService) {}

  ngOnInit(): void {
  }
  iAm()
  {
    this.childrenService.setHandleChildren$('je suis');
  }
  iAmNot()
  {
    this.childrenService.setHandleChildren$('je suis pas');
  }
  iIgnore()
  {
    this.childrenService.setHandleChildren$('je ignore');
  }
}
