                                                                                                                                                                                                                                import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LogoComponent } from './components/logo/logo.component';
import { Menu1Component } from './components/menu1/menu1.component';
                                                                                                                                                                                                                                import {ChildrenModule} from "./children/children.module";

@NgModule({
  declarations: [
    AppComponent,
    LogoComponent,
    Menu1Component
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        ChildrenModule
    ],
providers: [],
bootstrap: [AppComponent]
})
export class AppModule {




}
