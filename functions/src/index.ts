import * as functions from 'firebase-functions';
import * as admin from "firebase-admin";
import {cert} from "./lib/cert"

import DocumentSnapshot = admin.firestore.DocumentSnapshot;
import DocumentData = admin.firestore.DocumentData;
import {StudentModel} from "./models/student.model";


admin.initializeApp({
    credential: admin.credential.cert(cert),
    databaseURL: "https://schoolprojet-8ff99.firebaseio.com"
});
const db = admin.firestore();



export const getOneStudent = functions
    .region('europe-west3')
    .runWith({timeoutSeconds: 15, memory: '256MB'})
    .https
    .onRequest(async (req:any,res:DocumentData)=> {
        try {
            const {student} = req.body;
            if (!student) {
                throw new Error('student not found');
            }
            const colStudent: DocumentSnapshot = await db.collection('student').doc(student).get();
            const response: DocumentData | undefined = colStudent.data();
            if (!response) {
                throw new Error('student not exist');
            }
            return res.send(response);
        } catch (e) {
            console.error(e.message);
            return res.status(404);
        }
    });

export const getAllProfessors = functions
    .region('europe-west3')
    .runWith({timeoutSeconds: 15})
    .https
    .onRequest(async (req,res)=>{
        try {
            const {professor} = req.body;
            if (!professor) {
                throw new Error('professor not found');
            }
            const colProfessor: DocumentSnapshot = await db.collection('professors').doc(professor).get();
            const response: DocumentData | undefined = colProfessor.data();
            if (!response) {
                throw new Error('professor not exist');
            }
            return res.send(response);
        } catch (e) {
            console.error(e.message);
            return res.status(404);
        }
    });

export const addStudent = functions
    .region('europe-west3')
    .runWith({timeoutSeconds:15, memory: '256MB'})
    .https
    .onRequest(async (req:any,res:DocumentData) =>{
       const newStudent:StudentModel = req.body;
       console.log(newStudent);
        if(!newStudent ){throw new Error('new Student not found')}
       const colStudent:DocumentData = db.collection('student').add(newStudent);

        res.send(colStudent);
    })

export const getRefByProfessor = functions
    .region("europe-west3")
    .runWith({timeoutSeconds: 15, memory: '256MB'})
    .https
    .onRequest(async (req: any, res: any) => {
        const ComRefProf: DocumentSnapshot = await db.collection('professors')
            .doc('HVfhyE3KprY2cRj4WYf4')
            .collection('references').doc('5VQtpOP32oCpFusk6Zdz').get();

        const refProf = ComRefProf.data();
        if(!refProf){throw new Error('refProf not exist');}
        console.log(ComRefProf);
        res.send(ComRefProf);


    })

//export const helloWorld = functions.https.onRequest((request, response) => {
// response.send("Hello from Firebase!");
//});
